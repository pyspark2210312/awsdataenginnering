# Ingest S3 data into PostgresSQL database


### Prerequisite:
1. IAM User should have role starting with name AWSGlueServiceRole-<AnyName>
2. This role should have following permissions:
    - Administrator Access
    - AWSGlueConsoleFullAccess
    - CloudWatchLogsFullAccess
    - AWSLambdaVPCAccessExecutionRole
    - ResourceGroupsTagEditorFullAccess

    (Note: Actually when I was trying to perform operations related to Glue and S3, everytime I was getting permission issues. So I have done R&D each time and added relevant permission into the Role. So, at the end above all permissions were require to perform end to end operations)
3. Configure VPC > Endpoint for S3 Service. While configuring VPC > Endpoint, we have to give "Type=Gateway" to find the s3 endpoint during configuration and need to select other relevant config.
4. Glue Job will require connection to PostgreSQL. So connection should be set as "JDBC" while configuring Glue job.
5. Database should be created in PostgreSQL using AWS RDS service.
6. JDBC Connection should be created using databse configuration in which you will require Endpoint, Port, DB Name for JDBC URL Creation and also require username and password from the database while creating JDBC connection.
7. To Create JDBC URL follow link below:
https://aws.amazon.com/getting-started/hands-on/create-connect-postgresql-db/#:~:text=URL%3A%20You%20can%20find%20your,the%20end%20of%20the%20URL.
8. Create proper folder structure inside S3 bucket like:
    - raw-data (For storing raw data) 
    - script  (When Glue job requires script location, we can provide this location)
    - logs (When Glue job requires logs location, we can provide this location)
9. Database should be present in the Glue job.
10. PostgreSQL extension should be installed Azure Data Studio and should be configured. Given in Prerequisite folder snapshots.

## Project Steps
Ingest S3 data into PostgreSQL database in AWS RDS.

Steps I have done in this project as follows:
1. Created a table for CSV data stored in the S3 bucket using Crawlers in Glue service.
2. Created a Glue job which will take this Table (created from csv) as a source, apply mapping and ingest data into PostgreSQL database using JDBC connection.
3. Once table is created in the PostgreSQL, we can query that using Azure Data Studio (Needs to setup at our local system).
