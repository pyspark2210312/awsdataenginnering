# Transform CSV data into Parquet format in S3


### Prerequisite:
1. IAM User should have role starting with name AWSGlueServiceRole-<AnyName>
2. This role should have following permissions:
    - Administrator Access
    - AWSGlueConsoleFullAccess
    - CloudWatchLogsFullAccess
    - AWSLambdaVPCAccessExecutionRole
    - ResourceGroupsTagEditorFullAccess

    (Note: Actually when I was trying to perform operations related to Glue and S3, everytime I was getting permission issues. So I have done R&D each time and added relevant permission into the Role. So, at the end above all permissions were require to perform end to end operations)
3. Configure VPC > Endpoint for S3 Service. While configuring VPC > Endpoint, we have to give "Type=Gateway" to find the s3 endpoint during configuration and need to select other relevant config.
4. Glue Job will require connection to S3. So connection should be set as "Network" while configuring Glue job.
5. Create proper folder structure inside S3 bucket like:
    - raw-data (For storing raw data) 
    - transformed-data (For storing transformed data which is transformed using raw data)
    - script  (When Glue job requires script location, we can provide this location)
    - logs (When Glue job requires logs location, we can provide this location)
6. Database should be present in the Glue job.

## Project Steps
Transform CSV data into Parquet format in S3.

Steps I have done in this project as follows:
1. Created a table for CSV data stored in the S3 bucket using Crawlers in Glue service.
2. Created a Glue job which will take this Table (created from csv) as a source, apply mapping and transform into Parquet format and then store into the S3 bucket.
3. Created a table for Parquet data stored in the S3 bucket using Crawlers in Glue service.
4. Finally there are 2 tables in the Glue database. 1 for CSV and another for parquet. Read the data from these tables using the Athena service.
