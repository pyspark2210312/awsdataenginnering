import sys
import requests, json
import boto3

def main():
    baseURL = "https://api.openweathermap.org/data/2.5/weather?"
    cityName = "Toronto"
    apiKey = "de63c24edf5c181b6da102b8e2c444a9"

    completeURL = baseURL+"appid="+apiKey+"&q="+cityName

    response = requests.get(completeURL)
    jsonData = response.json()
    print(jsonData)

    s3Resource = boto3.resource('s3')
    s3Object = s3Resource.Object('project-ingest-api', 'api-raw/whether.json')

    s3Object.put(
        Body=(bytes(json.dumps(jsonData).encode('UTF-8')))
    )

if __name__ == "__main__":
    main()