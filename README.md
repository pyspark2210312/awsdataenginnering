# AWSDataEnginnering

### Projects:
1. Ingest data from REST API (Open Whether site) into S3 bucket as json and transform it into parquet format.
2. Upload CSV file into S3 bucket and transformed back into the S3 bucket as a parquet format file.
3. Ingest data into SQL database (PostgreSQL).


### Prerequisite:
1. IAM User should have role starting with name AWSGlueServiceRole-<AnyName>
2. This role should have following permissions:
    - Administrator Access
    - AWSGlueConsoleFullAccess
    - CloudWatchLogsFullAccess
    - AWSLambdaVPCAccessExecutionRole
    - ResourceGroupsTagEditorFullAccess

    (Note: Actually when I was trying to perform operations related to Glue and S3, everytime I was getting permission issues. So I have done R&D each time and added relevant permission into the Role. So, at the end above all permissions were require to perform end to end operations)
3. Configure VPC > Endpoint for S3 Service. While configuring VPC > Endpoint, we have to give "Type=Gateway" to find the s3 endpoint during configuration and need to select other relevant config.
4. Glue Job will require connection to S3. So connection should be set as "Network" while configuring Glue job.
5. Create proper folder structure inside S3 bucket like:
    - raw-data (For storing raw data) (Should have empty whether.json file in case of Project 1 at the begining of execution)
    - transformed-data (For storing transformed data which is transformed using raw data)
    - library (For storing python libraries) (Not required for CSV to parquet transformation)
    - script  (When Glue job requires script location, we can provide this location)
    - logs (When Glue job requires logs location, we can provide this location)

## Project 1
Ingest data from REST API (Open Whether site) into S3 bucket as json and transform it into parquet format.

Steps I have done in this project as follows:
1. Created "AWS Glue" job (for fetching whether data for Toronto city from OpenWhether website) using "Python Shell script editor" option > Written script for fetching data from API using requests library and saving that data into whether.json file of S3 bucket.
2. Created Table for whether.json file of S3 bucket using Crawler.
3. Created "AWS Glue" job (for transforming data from previous step table into parquet format and store it to target s3 location) using "Visual with a source and target". Source mapped with the Table created in the last step using Data Catalog option, Apply mapping with the Table and Target is set to S3 to save as a Parquet format. So parquet file saved into s3 at the end of this step.
4. Created Table for parquet file of S3 bucket using Crawler.
5. Used "AWS Athena" service to visualize the data.

## Project 2
Transform CSV data into Parquet format in S3.

Steps I have done in this project as follows:
1. Created a table for CSV data stored in the S3 bucket using Crawlers in Glue service.
2. Created a Glue job which will take this Table (created from csv) as a source, apply mapping and transform into Parquet format and then store into the S3 bucket.
3. Created a table for Parquet data stored in the S3 bucket using Crawlers in Glue service.
4. Finally there are 2 tables in the Glue database. 1 for CSV and another for parquet. Read the data from these tables using the Athena service.

## Project 3
Ingest S3 data into PostgreSQL database in AWS RDS.

Steps I have done in this project as follows:
1. Created a table for CSV data stored in the S3 bucket using Crawlers in Glue service.
2. Created a Glue job which will take this Table (created from csv) as a source, apply mapping and ingest data into PostgreSQL database using JDBC connection.
3. Once table is created in the PostgreSQL, we can query that using Azure Data Studio (Needs to setup at our local system).